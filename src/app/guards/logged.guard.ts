import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../services/auth.service';

@Injectable({ providedIn: 'root' })
export class LoggedGuard implements CanActivate {

    constructor(private router: Router,
        private cookieService: CookieService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (!this.cookieService.check('currentUser'))
            return true;

        // logged in so redirect to dashboard menu 
        this.router.navigate(['/devices']);
        return false;
    }
}