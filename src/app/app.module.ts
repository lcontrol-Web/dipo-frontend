import { LogComponent } from './components/logs/log/log.component';
import { WriteTagModalComponent } from './components/tags/write-tag-modal/write-tag-modal.component';
import { ConvertorComponent } from './components/convertor/convertor.component';
import { FilterSearchComponent } from './components/filter-search/filter-search.component';
import { AddEditTagModalComponent } from './components/tags/add-edit-tag-modal/add-edit-tag-modal.component';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';
import { AddEditDeviceModalComponent } from './components/devices/add-edit-device-modal/add-edit-device-modal.component';
import { ObjectTypePipe } from './pipes/object-type.pipe';
import { HumanizePipe } from './pipes/humanize.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { LoaderComponent } from './components/loader/loader.component';
import { ButtonLoaderComponent } from './components/button-loader/button-loader.component';
import { ImportModalComponent } from './components/import-modal/import-modal.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TagComponent } from './components/tags/tag/tag.component';
import { DeviceComponent } from './components/devices/device/device.component';
import { TopNavbarComponent } from './components/top-navbar/top-navbar.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { NgxPaginationModule } from 'ngx-pagination';
import DeactivateGuard from './guards/deactivate.guard';
import { DuplicateTagModalComponent } from './components/tags/duplicate-tag-modal/duplicate-tag-modal.component';
import { FilterHeaderComponent } from './components/filter-header/filter-header.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TopNavbarComponent,
    DeleteModalComponent,
    DeviceComponent,
    AddEditDeviceModalComponent,
    TagComponent,
    AddEditTagModalComponent,
    WriteTagModalComponent,
    DuplicateTagModalComponent,
    ConvertorComponent,
    ImportModalComponent,
    ButtonLoaderComponent,
    LoaderComponent,
    FilterSearchComponent,
    LogComponent,
    FilterHeaderComponent,

    ObjectTypePipe,
    FilterPipe,
    HumanizePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [
    CookieService,
    DeactivateGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
