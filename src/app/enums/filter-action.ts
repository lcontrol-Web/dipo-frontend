export enum FilterAction {
    Filter = 1,
    Sort = 2,
    Clear = 3,
}