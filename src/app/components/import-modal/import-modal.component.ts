import { ModbusTagService } from './../../services/modbus-tag.service';
import { ModbusService } from 'src/app/services/modbus.service';
import { ModalMessage } from './../../models/modal-message';
import { ModalService } from 'src/app/services/modal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, SubscriptionLike } from 'rxjs';
import { TagService } from './../../services/tag.service';
import { DeviceService } from './../../services/device.service';
import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import * as XLSX from 'xlsx';
declare var $: any;

@Component({
  selector: 'app-import-modal',
  templateUrl: './import-modal.component.html',
  styleUrls: ['./import-modal.component.css']
})
export class ImportModalComponent implements OnInit {

  public type: string = '';
  public errorImport: any[] = [];

  public file: any;
  public fileName: string = '';
  public isLoading: boolean = false;

  private arrayBuffer: any;
  private locationSubscription: SubscriptionLike;

  constructor(private deviceService: DeviceService,
    private tagService: TagService,
    private modbusTagService: ModbusTagService,
    private modalService: ModalService,
    private modbusService: ModbusService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute) {
    this.locationSubscription = this.location.subscribe(location => {
      $('#importModal').modal('hide');
    });
  }

  ngOnInit() {
    $('#importModal').modal('show');
    this.type = this.route.snapshot.queryParamMap.get('type')!;
  }

  ngOnDestroy() {
    this.locationSubscription.unsubscribe();
  }

  public navigate() {
    $('#importModal').modal('hide');
    setTimeout(() => {
      this.router.navigate(['../'], { relativeTo: this.route });
    }, 200);
  }

  public fileChanged(event: any) {
    this.file = event.target.files[0];
    this.fileName = this.file.name;
  }

  private updateModbus() {
    this.modbusService.refreshService()
      .subscribe(
        (response: any) => {
          
        },
        (error: string) => {

        });
  }

  public import() {
    try {
      this.isLoading = true;
      let fileReader = new FileReader();
      fileReader.readAsArrayBuffer(this.file);
      fileReader.onload = (e) => {
        this.arrayBuffer = fileReader.result;
        var data = new Uint8Array(this.arrayBuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, { type: "binary" });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        // console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
        var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
        let subscription!: Observable<any[]>;

        switch (this.type) {
          case 'devices':
            subscription = this.deviceService.import(arraylist);
            break;
          case 'tags':
            subscription = this.tagService.import(arraylist);
            break;
          case 'convertor':
            subscription = this.modbusTagService.importAddress(arraylist);
            break;
        }


        subscription
          .subscribe(
            (response: any[]) => {
              if (response.length > 0) {
                this.isLoading = false
                this.errorImport = response;
              }
              else {
                this.updateModbus();
                this.modalService.changesMade(new ModalMessage('import', null));
                this.navigate();
              }
            },
            (error: string) => {
              this.isLoading = false
            })
      }
    }
    catch (error) {
      this.isLoading = false
    }
  }
}
