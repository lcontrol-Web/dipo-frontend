export class Tag {
    public tagId: number = 0;
    public tagName: string = '';
    public deviceId: number = 0;
    public objectType: number = 1;
    public id: number = 1;
    public indexArray: number = 1;
    public bitOffset: number = 0;
    public amount: number = 0;
    public calculator: string = 'X';
    public timeSearch: number = 20;
    public minValueToWrite: number = 0;
    public maxValueToWrite: number = 1;
    public enableWriting: boolean = false;
    public calculatedValue?: number = undefined;
    public value?: number = undefined;
    public valueToWrite?: number = undefined;
    public deviceName?: string = undefined;
    public address?: number = undefined;
}

export class ModbusTag {
    public tagId: number = 0;
    public tagName: string = '';
    public deviceId: number = 0;
    public objectType: number = 1;
    public id: number = 1;
    public indexArray: number = 1;
    public bitOffset: number = 0;
    public deviceName: string = '';
    public address!: number;
    public addressChanged: boolean = false;
    public realAddress!: number;
    public tagWithSameAddress?: number;
}

export class UpdatedModbusTag {
    public tagId: number = 0;
    public address!: number;
}

export class TagValue {
    public tagId: number = 0;
    public value!: number;
    public calculatedValue!: number;
}

export class DuplicateTag {
    public amount: number = 1;
    public startFrom: number = 1;
    public duplicateBy: number = 1;
    public steps: number = 1;
    public tagId!: number;
}
