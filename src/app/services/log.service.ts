import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Log } from '../models/log';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private dataService: DataService) { }

  public getLogs(): Observable<Log[]> {
    this.dataService.apiName = 'logs';
    return this.dataService.getData();
  }


}
