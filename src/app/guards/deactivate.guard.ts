import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { ConvertorComponent } from '../components/convertor/convertor.component';

@Injectable()
export default class DeactivateGuard implements CanDeactivate<ConvertorComponent> {

    canDeactivate(convertorComponent: ConvertorComponent) {
        let can = convertorComponent.canDeactivate();

        if (can) {
            if (confirm('There are unsaved changes, are you sure you want to leave this page?'))
                return true;
            else
                return false;
        }

        return true;
    }

}