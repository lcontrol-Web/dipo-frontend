import { Device } from './../../models/device';
import { ModbusService } from 'src/app/services/modbus.service';
import { ModalService } from './../../services/modal.service';
import { SubscriptionLike } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { ModalMessage } from 'src/app/models/modal-message';
import { Location } from "@angular/common";
declare var $: any;

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  public isLoading: boolean = false;
  public type: string = '';
  public id: number = 0;
  public deviceId: number = 0;
  public name: string = '';
  public errorMessage: string = '';

  private locationSubscription: SubscriptionLike;

  constructor(private dataService: DataService,
    private modalService: ModalService,
    private modbusService: ModbusService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute) {
    this.locationSubscription = this.location.subscribe(location => {
      $('#deleteModal').modal('hide');
    });
  }

  ngOnInit() {
    $('#deleteModal').modal('show');
    this.type = this.route.snapshot.queryParamMap.get('type')!;
    this.id = +this.route.snapshot.queryParamMap.get('id')!;
    this.deviceId = +this.route.snapshot.queryParamMap.get('deviceId')!;
    this.name = this.route.snapshot.queryParamMap.get('name')!;
  }

  ngOnDestroy() {
    this.locationSubscription.unsubscribe();
  }

  public navigate() {
    $('#deleteModal').modal('hide');
    setTimeout(() => {
      this.router.navigate(['../'], { relativeTo: this.route });
    }, 200);
  }

  private updateModbusDevice(deviceId: number) {
    this.modbusService.updateDevice(deviceId)
      .subscribe(
        (response: any) => {

        },
        (error: string) => {

        })
  }

  public delete() {
    this.isLoading = true;
    this.errorMessage = '';

    this.dataService.apiName = `${this.type}`
    this.dataService.delete(this.id)
      .subscribe(
        (item: any) => {
          this.updateModbusDevice(this.deviceId);
          this.modalService.changesMade(new ModalMessage('delete', item));
          this.navigate();
        },
        (error: string) => {
          this.errorMessage = error;
          this.isLoading = false;
        })
  }
}
