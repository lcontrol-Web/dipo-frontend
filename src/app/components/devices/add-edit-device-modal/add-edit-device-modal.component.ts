import { ModbusService } from './../../../services/modbus.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, SubscriptionLike } from 'rxjs';
import { Device } from 'src/app/models/device';
import { ModalMessage } from 'src/app/models/modal-message';
import { DeviceService } from 'src/app/services/device.service';
import { ModalService } from 'src/app/services/modal.service';
import { Location } from "@angular/common";
declare var $: any;

@Component({
  selector: 'app-add-edit-device-modal',
  templateUrl: './add-edit-device-modal.component.html',
  styleUrls: ['./add-edit-device-modal.component.css']
})
export class AddEditDeviceModalComponent implements OnInit {

  public deviceForm: FormGroup = this.fb.group({
    deviceName: ['', Validators.required],
    ip: ['', Validators.required],
    port: [502, Validators.required],
    timeSearch: [400, Validators.required],
    timeOut: [10, Validators.required],
    protocol: ['ModbusTcpIp', Validators.required],
    maxRegisterToRead: [16, Validators.required],
    enable: [true, Validators.required]
  })

  public isLoading: boolean = false;
  public dataIsReady: boolean = false;
  public isAddMode: boolean = false;
  public readOptions: number[] = [1, 2, 4, 8, 16, 32, 64, 125, 128];
  public deviceId: number = 0;
  public errorMessage: string = '';

  private locationSubscription: SubscriptionLike;

  constructor(private deviceService: DeviceService,
    private modalService: ModalService,
    private modbusService: ModbusService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder) {

    this.locationSubscription = this.location.subscribe(location => {
      $('#addEditModal').modal('hide');
    });
  }

  ngOnInit() {
    $('#addEditModal').modal('show');

    // edit
    if (this.route.snapshot.paramMap.get('id')) {
      this.deviceId = +this.route.snapshot.paramMap.get('id')!;
      this.deviceService.getDeviceByDeviceId(this.deviceId)
        .subscribe(
          (device: Device) => {
            this.initializeForm(device);
            this.dataIsReady = true;
          },
          (error: string) => {
            this.isLoading = false;
          });
    }
    // add
    else {
      this.isAddMode = true;
      this.dataIsReady = true;
    }
  }

  ngOnDestroy() {
    this.locationSubscription.unsubscribe();
  }

  public navigate() {
    $('#addEditModal').modal('hide');
    setTimeout(() => {
      if (this.isAddMode)
        this.router.navigate(['../'], { relativeTo: this.route });
      else
        this.router.navigate(['../../'], { relativeTo: this.route });
    }, 150);
  }

  private initializeForm(device: Device) {
    this.deviceForm.setValue({
      deviceName: device.deviceName,
      ip: device.ip,
      port: device.port,
      timeSearch: device.timeSearch,
      timeOut: device.timeOut,
      protocol: device.protocol,
      maxRegisterToRead: device.maxRegisterToRead,
      enable: device.enable,
    });
  }

  private addModbusDevice(deviceId: number) {
    this.modbusService.addDevice(deviceId)
      .subscribe(
        (response: any) => {

        },
        (error: string) => {

        })
  }
  private updateModbusDevice(deviceId: number) {
    this.modbusService.updateDevice(deviceId)
      .subscribe(
        (response: any) => {

        },
        (error: string) => {

        })
  }

  public onSubmit() {
    this.isLoading = true;
    this.errorMessage = '';
    let httpRequest: Observable<Device>;

    if (this.deviceId)
      httpRequest = this.deviceService.editDevice(this.deviceId, this.deviceForm.value)
    else
      httpRequest = this.deviceService.addDevice(this.deviceForm.value)

    httpRequest
      .subscribe(
        (device: Device) => {
          if (this.isAddMode)
            this.addModbusDevice(device.deviceId);
          else
            this.updateModbusDevice(this.deviceId);

          this.modalService.changesMade(new ModalMessage(this.isAddMode ? 'add' : 'edit', device));
          this.navigate();
        },
        (error: string) => {
          this.errorMessage = error;
          this.isLoading = false;
        });
  }
}