import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ModbusTag, UpdatedModbusTag } from '../models/tag';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class ModbusTagService {

  constructor(private dataService: DataService) { }

  public getModbusTags(): Observable<ModbusTag[]> {
    this.dataService.apiName = 'modbus-tags';
    return this.dataService.getData();
  }

  public updateModbusTags(modbusTags: UpdatedModbusTag[]): Observable<UpdatedModbusTag[]> {
    this.dataService.apiName = 'modbus-tags';
    return this.dataService.add(modbusTags);
  }

  public importAddress(tags: any[]): Observable<any[]> {
    this.dataService.apiName = 'modbus-tags/import-address';
    return this.dataService.add(tags);
  }
}