export class Credentials {
    public userName: string = '';
    public password: string = '';
    public remember: boolean = false;
}
