export class Log {
    public tagId: number = 0;
    public tagName: string = '';
    public deviceId: number = 0;
    public deviceName: number = 0;
    public objectType: number = 1;
    public id: number = 1;
    public indexArray: number = 1;
    public bitOffset: number = 0;
    public value?: number;
    public time?: Date;
}
