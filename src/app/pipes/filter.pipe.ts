import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPipe'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], value: any, field: string, args?: any): any {
    if (!items || !value || !field || value === -1) {
      return items;
    }

    return items.filter(item => item[field] === value);
  }

}
