import { TagService } from './../../../services/tag.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModbusService } from './../../../services/modbus.service';
import { Tag } from './../../../models/tag';
import { Component, OnInit } from '@angular/core';
import { SubscriptionLike } from 'rxjs';
import { Location } from "@angular/common";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-write-tag-modal',
  templateUrl: './write-tag-modal.component.html',
  styleUrls: ['./write-tag-modal.component.css']
})
export class WriteTagModalComponent implements OnInit {

  public writeForm: FormGroup = this.fb.group({
    value: ['', Validators.required],
  });

  public isWriting: boolean = false;
  public dataIsReady: boolean = false;
  public errorWriting: boolean = false;
  public valueToWrite: number = 0;
  public tag!: Tag;
  public tagId!: number;

  private locationSubscription: SubscriptionLike;

  constructor(private modbusService: ModbusService,
    private tagService: TagService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location) {
    this.locationSubscription = this.location.subscribe(location => {
      $('#writeModal').modal('hide');
    });
  }

  ngOnInit() {
    $('#writeModal').modal('show');
    if (this.route.snapshot.paramMap.get('id')) {
      this.tagId = +this.route.snapshot.paramMap.get('id')!;
      this.tagService.getTagByTagId(this.tagId)
        .subscribe(
          (tag: Tag) => {
            this.tag = tag;
            this.writeForm.controls['value'].setValidators([
              Validators.min(this.tag.minValueToWrite),
              Validators.max(this.tag.maxValueToWrite),
              Validators.required
            ]);

            this.dataIsReady = true;
          },
          (error: string) => {

          });
    }
    else {

    }
  }

  ngOnDestroy() {
    this.locationSubscription.unsubscribe();
  }

  public navigate() {
    $('#writeModal').modal('hide');
    setTimeout(() => {
      this.router.navigate(['../../'], { relativeTo: this.route });
    }, 200);
  }

  public onSubmit() {
    this.isWriting = true;
    this.errorWriting = false;
    this.tag.valueToWrite = this.writeForm.get('value')?.value;
    this.modbusService.writeToTag(this.tag)
      .subscribe(
        (response: any) => {
          this.navigate();
        },
        (error: string) => {
          this.isWriting = false;
          this.errorWriting = true;
        });
  }

}
