import { FilterHeaderService } from './../../services/filter-header.service';
import { ModbusService } from 'src/app/services/modbus.service';
import { Router } from '@angular/router';
import { ModbusTagService } from './../../services/modbus-tag.service';
import { ModbusTag, UpdatedModbusTag } from './../../models/tag';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Device } from 'src/app/models/device';
import { DeviceService } from 'src/app/services/device.service';
import { FilterSearchComponent } from '../filter-search/filter-search.component';
import { forkJoin, Subscription } from 'rxjs';
import { ModalMessage } from 'src/app/models/modal-message';
import { ModalService } from 'src/app/services/modal.service';
declare var $: any;

@Component({
  selector: 'app-convertor',
  templateUrl: './convertor.component.html',
  styleUrls: ['./convertor.component.css']
})
export class ConvertorComponent implements OnInit {

  @ViewChild(FilterSearchComponent) filterSearchComponent!: FilterSearchComponent;

  public p: number = 1;
  public itemsPerPage: number = 1000;
  public itemsPerPageOptions: number[] = [10, 25, 50, 100, 1000];
  public dataIsReady: boolean = false;
  public objectTypes: number[] = [1, 2, 3, 4];
  public devices: Device[] = [];
  public tags: ModbusTag[] = [];
  public filteredTags: ModbusTag[] = [];

  public isLoading: boolean = false;
  public changesMade: boolean = false;
  public selectedObjectType: number = -1;
  public selectedDevice: number = -1;
  public duplicateAddressError: boolean = false;
  public tagWithSameAddress!: ModbusTag;

  private modalSubscription: Subscription;

  constructor(private modbusTagService: ModbusTagService,
    private deviceService: DeviceService,
    private modalService: ModalService,
    private modbusService: ModbusService,
    private filterHeaderService: FilterHeaderService,
    private router: Router) {
    this.modalSubscription = this.modalService.onChangesMade()
      .subscribe(
        (message: ModalMessage) => {
          this.dataIsReady = false;

          this.filterSearchComponent.clearSearch();
          this.filterHeaderService.clearSearch();
          this.getData();
        });
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    if (this.modalSubscription)
      this.modalSubscription.unsubscribe();
  }

  private getData() {
    forkJoin([this.modbusTagService.getModbusTags(), this.deviceService.getDevices()])
      .subscribe(
        (response: [ModbusTag[], Device[]]) => {
          this.tags = response[0];
          this.devices = response[1];
          this.filterTags();
          this.dataIsReady = true
        },
        (error: string) => {
          //this.errorMessage = error;
        });
  }

  public objectTypeChanged() {
    this.dataIsReady = false;
    this.p = 1;
    this.filterTags();
    this.filterSearchComponent.clearSearch();
    this.filterHeaderService.clearSearch();
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public deviceChanged() {
    this.dataIsReady = false;
    this.p = 1;
    this.filterSearchComponent.clearSearch();
    this.filterHeaderService.clearSearch();
    this.selectedObjectType = -1;
    this.filterTags();
    this.setObjectTypes();

    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  private setObjectTypes() {
    this.objectTypes = [...new Set(this.filteredTags.map((tag: ModbusTag) => tag.objectType))];
  }

  private refreshService() {
    this.modbusService.refreshService()
      .subscribe(
        (response: any) => {

        },
        (error: string) => {

        });
  }

  public save() {
    this.dataIsReady = false;
    this.duplicateAddressError = false;

    $(document).ready(() => {
      let exists = this.checkAddresses();
      if (exists) {
        this.dataIsReady = true;
        this.duplicateAddressError = true;
        return;
      }

      let updatedTags: UpdatedModbusTag[] = [];
      for (const tag of this.tags)
        if (tag.addressChanged)
          updatedTags.push({ tagId: tag.tagId, address: tag.address });

      this.modbusTagService.updateModbusTags(updatedTags)
        .subscribe(
          (response: any) => {
            this.changesMade = false;
            this.refreshService();
            this.getData();
          },
          (error: string) => {

          });
    });
  }

  public navigate(event: KeyboardEvent, index: number) {
    if (event.key === 'ArrowDown' || event.key === 'ArrowUp') {
      event.preventDefault();
      $(`#inputAddress_${event.key === 'ArrowUp' ? --index : ++index}`).focus();
    }
  }

  public addressChanged(tag: ModbusTag) {
    this.changesMade = true;
    this.duplicateAddressError = false;
    tag.tagWithSameAddress = undefined;

    if (tag.realAddress != tag.address)
      tag.addressChanged = true;
    else
      tag.addressChanged = false;

    let found = this.tags.some((tag: ModbusTag) => tag.addressChanged);
    if (!found)
      this.changesMade = false;
  }

  private checkAddresses(): boolean {
    let flag = false;
    for (let tag of this.tags) {
      let index = this.tags.findIndex(el => el.address &&
        tag.address &&
        el.tagId !== tag.tagId &&
        el.address === tag.address);
      if (index !== -1) {
        tag.tagWithSameAddress = this.tags[index].tagId;
        flag = true;
      }
      else
        tag.tagWithSameAddress = undefined;
    }

    return flag;
  }

  public showTagWithSameAddress(tag: ModbusTag) {
    const index = this.tags.findIndex(el => el.tagId == tag.tagWithSameAddress);
    this.tagWithSameAddress = this.tags[index];
  }

  public import() {
    this.router.navigate(['/convertor/import'], {
      queryParams: {
        type: 'convertor',
      }
    });
  }

  public canDeactivate(): boolean {
    return this.changesMade;
  }

  public onFilterHeaderStart() {
    this.filterSearchComponent.clearSearch();
    this.dataIsReady = false;
  }

  public onFilterHeaderFinish(items: any[]) {
    this.p = 1;
    this.selectedDevice == -1;
    this.selectedObjectType == -1;
    this.filteredTags = items;
    this.dataIsReady = true;
  }

  public itemsPerPageChanged() {
    this.dataIsReady = false;
    this.p = 1;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  private filterTags() {
    if (this.selectedDevice == -1 && this.selectedObjectType == -1)
      this.filteredTags = this.tags;
    else if (this.selectedDevice == -1 && this.selectedObjectType != -1) {
      this.filteredTags = this.tags;
      this.filteredTags = this.tags.filter((tag: ModbusTag) => tag.objectType == this.selectedObjectType);
    }
    else if (this.selectedDevice != -1 && this.selectedObjectType == -1) {
      this.filteredTags = this.tags;
      this.filteredTags = this.tags.filter((tag: ModbusTag) => tag.deviceId == this.selectedDevice);
    }
    else
      this.filteredTags = this.tags.filter((tag: ModbusTag) => tag.deviceId == this.selectedDevice && tag.objectType == this.selectedObjectType);
  }

  public onSearch(filteredItems: ModbusTag[]) {
    this.p = 1;
    this.selectedDevice = -1;
    this.selectedObjectType = -1;
    this.filteredTags = filteredItems;
    this.dataIsReady = true;
  }

  public searchStarted() {
    this.filterHeaderService.clearSearch();
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.p = page;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }
}