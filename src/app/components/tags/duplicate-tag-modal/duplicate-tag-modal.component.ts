import { ModalMessage } from './../../../models/modal-message';
import { ModalService } from './../../../services/modal.service';
import { ModbusService } from './../../../services/modbus.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { Location } from "@angular/common";
import { Tag } from 'src/app/models/tag';
import { TagService } from 'src/app/services/tag.service';
declare var $: any;

@Component({
  selector: 'app-duplicate-tag-modal',
  templateUrl: './duplicate-tag-modal.component.html',
  styleUrls: ['./duplicate-tag-modal.component.css']
})
export class DuplicateTagModalComponent implements OnInit {

  public duplicateForm: FormGroup = this.fb.group({
    tagId: [1],
    amount: [1, Validators.required],
    startFrom: [1, Validators.required],
    duplicateBy: [1, Validators.required],
    steps: [1, Validators.required],
  });

  public isDuplicating: boolean = false;
  public dataIsReady: boolean = false;
  public errorDuplicating: boolean = false;
  public valueToWrite: number = 0;
  public tag!: Tag;
  public tagId!: number;

  private locationSubscription: SubscriptionLike;

  constructor(private tagService: TagService,
    private modbusService: ModbusService,
    private modalService: ModalService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location) {
    this.locationSubscription = this.location.subscribe(location => {
      $('#duplicateModal').modal('hide');
    });
  }

  ngOnInit() {
    $('#duplicateModal').modal('show');
    if (this.route.snapshot.paramMap.get('id')) {
      this.tagId = +this.route.snapshot.paramMap.get('id')!;
      this.tagService.getTagByTagId(this.tagId)
        .subscribe(
          (tag: Tag) => {
            this.tag = tag;
            this.duplicateForm.controls['tagId'].setValue(this.tagId);
            this.duplicateForm.controls['startFrom'].setValue(this.tag.indexArray + 1);
            this.dataIsReady = true;
          },
          (error: string) => {

          });
    }
    else {

    }
  }

  ngOnDestroy() {
    this.locationSubscription.unsubscribe();
  }

  public navigate() {
    $('#duplicateModal').modal('hide');
    setTimeout(() => {
      this.router.navigate(['../../'], { relativeTo: this.route });
    }, 200);
  }

  public duplicateByChanged() {
    switch (+this.duplicateForm.controls['duplicateBy'].value) {
      case 1:
        this.duplicateForm.controls['startFrom'].setValue(this.tag.indexArray + 1);
        break;
      case 2:
        this.duplicateForm.controls['startFrom'].setValue(this.tag.id + 1);
        break;
      case 3:
        this.duplicateForm.controls['startFrom'].setValue(this.tag.bitOffset + 1);
        break;
      default:
        break;
    }
  }

  private updateModbusDevice(deviceId: number) {
    this.modbusService.updateDevice(deviceId)
      .subscribe(
        (response: any) => {

        },
        (error: string) => {

        })
  }

  public onSubmit() {
    this.isDuplicating = true;
    this.errorDuplicating = false;
    this.tagService.duplicate(this.duplicateForm.value)
      .subscribe(
        (response: any) => {
          this.updateModbusDevice(this.tag.deviceId);
          this.modalService.changesMade(new ModalMessage('duplicate', this.tag));
          this.navigate();
        },
        (error: string) => {
          this.isDuplicating = false;
          this.errorDuplicating = true;
        });
  }

}
