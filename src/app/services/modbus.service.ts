import { Tag, TagValue } from './../models/tag';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DeviceStatus } from '../models/device';
import { DataModbusService } from './data-modbus.service';

@Injectable({
  providedIn: 'root'
})
export class ModbusService {

  constructor(private dataModbusService: DataModbusService) { }

  public getDevicesStatus(): Observable<DeviceStatus[]> {
    this.dataModbusService.apiName = 'devices/status';
    return this.dataModbusService.getData();
  }

  public refreshService(): Observable<any> {
    this.dataModbusService.apiName = 'devices/refresh';
    return this.dataModbusService.getData();
  }

  public addDevice(deviceId: number): Observable<any> {
    this.dataModbusService.apiName = 'devices/add-device';
    return this.dataModbusService.getDataByParam(deviceId);
  }

  public updateDevice(deviceId: number): Observable<any> {
    this.dataModbusService.apiName = 'devices/update-device';
    return this.dataModbusService.getDataByParam(deviceId);
  }

  public getTagsValues(tags: any[]): Observable<TagValue[]> {
    this.dataModbusService.apiName = 'tags/values';
    return this.dataModbusService.getDataByObject(tags);
  }

  public writeToTag(tag: Tag): Observable<number> {
    this.dataModbusService.apiName = 'tags/write';
    return this.dataModbusService.getDataByObject(tag);
  }
  
}