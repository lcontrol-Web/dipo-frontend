import { FilterHeaderMessage } from './../models/filter-header-message';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { FilterAction } from '../enums/filter-action';

@Injectable({
  providedIn: 'root'
})
export class FilterHeaderService {

  private subject = new Subject<any>();

  constructor() { }

  sendMessage(message: FilterHeaderMessage) {
    this.subject.next(message);
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  onSort(filterNumber: number) {
    this.sendMessage({
      action: FilterAction.Sort,
      filterNumber: filterNumber
    });
  }

  onFilter(filterNumber: number) {
    this.sendMessage({
      action: FilterAction.Filter,
      filterNumber: filterNumber
    });
  }

  clearSearch() {
    this.sendMessage({ action: FilterAction.Clear });
  }

  public asc(items: any[], name: string, type: string): any[] {
    
    const x = [...items].sort((a, b) => {
      let fa, fb;

      if (type === 'string') {
        if (a.hasOwnProperty(name) && a[name])
          fa = a[name].toString().toLowerCase();
        if (b.hasOwnProperty(name) && b[name])
          fb = b[name].toString().toLowerCase();
      }
      else if (type === 'number') {
        if (a.hasOwnProperty(name) && a[name])
          fa = +a[name];
        if (b.hasOwnProperty(name) && b[name])
          fb = +b[name];
      }
      
   
      if (fa < fb || fa === null || fa === undefined) {
        return -1;
      }

      if (fa > fb || fb === null || fb === undefined) {
        return 1;
      }
      
      return 0;
    });
    
    return x;
  }

  public dsc(items: any[], name: string, type: string): any[] {

    return [...items].sort((a, b) => {

      let fa, fb;
      if (type === 'string') {
        if (a.hasOwnProperty(name) && a[name])
          fa = a[name].toString().toLowerCase();
        if (b.hasOwnProperty(name) && b[name])
          fb = b[name].toString().toLowerCase();
      }
      else if (type === 'number') {
        if (a.hasOwnProperty(name) && a[name])
          fa = +a[name];
        if (b.hasOwnProperty(name) && b[name])
          fb = +b[name];
      }

      if (fa < fb || fa === null || fa === undefined) {
        return 1;
      }

      if (fa > fb || fb === null || fb === undefined) {
        return -1;
      }
      
      return 0;
    });
  }

}
