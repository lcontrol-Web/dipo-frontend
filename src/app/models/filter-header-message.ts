import { FilterAction } from './../enums/filter-action';
export class FilterHeaderMessage {
    public action!: FilterAction;
    public filterNumber?: number;
}
