import { FilterHeaderService } from './../../../services/filter-header.service';
import { ModbusService } from './../../../services/modbus.service';
import { ModalMessage } from './../../../models/modal-message';
import { FilterSearchComponent } from './../../filter-search/filter-search.component';
import { DeviceService } from 'src/app/services/device.service';
import { Device, DeviceStatus } from './../../../models/device';
import { ModalService } from './../../../services/modal.service';
import { Router } from '@angular/router';
import { TagService } from './../../../services/tag.service';
import { Tag, TagValue } from './../../../models/tag';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription, forkJoin, interval } from 'rxjs';
import { startWith, switchMap, retry } from 'rxjs/operators';
import { FilterAction } from 'src/app/enums/filter-action';
declare var $: any;

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  @ViewChild(FilterSearchComponent) filterSearchComponent!: FilterSearchComponent;

  public p: number = 1;
  public itemsPerPage: number = 1000;
  public itemsPerPageOptions: number[] = [10, 25, 50, 100, 1000];
  public dataIsReady: boolean = false;
  public modbusServiceIsDown: boolean = false;
  public objectTypes: number[] = [1, 2, 3, 4];
  public devices: Device[] = [];
  public tags: Tag[] = [];
  public filteredTags: Tag[] = [];

  public selectedObjectType: number = -1;
  public selectedDeviceId: number = -1;
  public selectedDevice!: Device | null;
  public selectedTag!: Tag;

  public isWriting: boolean = false;
  public errorWriting: boolean = false;
  public valueToWrite: number = 0;
  public lastAddedTagId?: number;

  private modalSubscription: Subscription;
  private deviceStatusInterval!: Subscription;
  private tagsValuesInterval!: Subscription;

  constructor(private tagService: TagService,
    private deviceService: DeviceService,
    private modbusService: ModbusService,
    private modalService: ModalService,
    private filterHeaderService: FilterHeaderService,
    private router: Router) {

    this.modalSubscription = this.modalService.onChangesMade()
      .subscribe(
        (message: ModalMessage) => {
          this.dataIsReady = false;

          if (message.modalType == 'add')
            this.lastAddedTagId = (<number>message.item.tagId);

          this.filterSearchComponent.clearSearch();
          this.filterHeaderService.clearSearch();
          this.getData();
        });
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this.modalSubscription ? this.modalSubscription.unsubscribe() : null;
    this.deviceStatusInterval ? this.deviceStatusInterval.unsubscribe() : null;
    this.tagsValuesInterval ? this.tagsValuesInterval.unsubscribe() : null;
  }

  private getData() {
    forkJoin([this.tagService.getTags(), this.deviceService.getDevices()])
      .subscribe(
        (response: [Tag[], Device[]]) => {
          this.tags = response[0];
          this.devices = response[1];
          this.filterTags();
          this.getDevicesStatus();
          this.getTagsValues();

          // if deleted last one
          if (this.p > Math.round(this.filteredTags.length / this.itemsPerPage))
            this.p = this.p > 1 ? --this.p : 1;

          this.dataIsReady = true
        },
        (error: string) => {

        });
  }

  public addTag() {
    this.router.navigate(['/tags/add'], { queryParams: { lastAddedTagId: this.lastAddedTagId } });
  }

  public editTag(tag: Tag) {
    this.router.navigate(['/tags/edit', tag.tagId]);
  }

  public import() {
    this.router.navigate(['/tags/import'], {
      queryParams: {
        type: 'tags',
      }
    });
  }

  public duplicate(tag: Tag) {
    this.router.navigate(['/tags/duplicate', tag.tagId]);
  }

  public deleteTag(tag: Tag) {
    this.router.navigate(['/tags/delete'], {
      queryParams: {
        type: 'tags',
        id: tag.tagId,
        deviceId: tag.deviceId,
        name: tag.tagName
      }
    });
  }

  public objectTypeChanged() {
    this.dataIsReady = false;
    this.p = 1;
    this.filterTags();
    this.getDevicesStatus();
    this.filterSearchComponent.clearSearch();
    this.filterHeaderService.clearSearch();
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public deviceChanged() {
    this.dataIsReady = false;
    this.selectedDevice = this.devices.find(device => device.deviceId == this.selectedDeviceId)!;
    this.p = 1;
    this.filterSearchComponent.clearSearch();
    this.filterHeaderService.clearSearch();
    this.selectedObjectType = -1;
    this.filterTags();
    this.setObjectTypes();
    this.getTagsValues();
    this.getDevicesStatus();
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public writeTag(tag: Tag) {
    this.router.navigate(['/tags/write', tag.tagId]);
  }

  private setObjectTypes() {
    this.objectTypes = [...new Set(this.filteredTags.map((tag: Tag) => tag.objectType))];
  }

  private resetTagsValues() {
    for (const tag of this.filteredTags) {
      tag.value = undefined;
      tag.calculatedValue = undefined;
    }
  }

  private resetDevicesStatus() {
    for (const device of this.devices)
      device.status = false;
  }

  private getDevicesStatus() {
    if (this.deviceStatusInterval)
      this.deviceStatusInterval.unsubscribe();

    this.deviceStatusInterval = interval(2000)
      .pipe(
        startWith(0),
        switchMap(() => this.modbusService.getDevicesStatus()),
        retry(3))
      .subscribe(
        (devicesStatus: DeviceStatus[]) => {
          this.modbusServiceIsDown = false;

          for (const deviceStatus of devicesStatus)
            for (let device of this.devices)
              if (deviceStatus.deviceId == device.deviceId) {
                device.status = deviceStatus.status;

                if (this.selectedDeviceId == device.deviceId && !device.status)
                  this.resetTagsValues();
              }
        },
        (error: string) => {
          this.resetDevicesStatus();
          this.resetTagsValues();

          if (error == "ServerIsDown")
            this.modbusServiceIsDown = true
        })
  }

  private getTagsValues() {
    if (this.tagsValuesInterval)
      this.tagsValuesInterval.unsubscribe();

    this.tagsValuesInterval = interval(2000)
      .pipe(
        startWith(0),
        switchMap((val) => {
          let tags = this.filteredTags.slice((this.p - 1) * this.itemsPerPage, this.p * this.itemsPerPage)
            .map((tag: Tag) => {
              return {
                tagId: tag.tagId,
                deviceId: tag.deviceId
              }
            });
          return this.modbusService.getTagsValues(tags)
        }),
        retry(3))
      .subscribe(
        (tagsValues: TagValue[]) => {
          this.modbusServiceIsDown = false;

          for (const tagValue of tagsValues) {
            for (const tag of this.filteredTags) {
              if (tagValue.tagId == tag.tagId) {
                tag.value = tagValue.value;
                tag.calculatedValue = tagValue.calculatedValue;
                break;
              }
            }
          }
        },
        (error: string) => {
          this.resetTagsValues();
          if (error == "ServerIsDown")
            this.modbusServiceIsDown = true
        });
  }

  public onFilterHeaderStart() {
    this.filterSearchComponent.clearSearch();
    this.dataIsReady = false;
  }

  public onFilterHeaderFinish(items: any[]) {
    this.p = 1;
    this.selectedDeviceId == -1;
    this.selectedDevice = null;
    this.selectedObjectType == -1;
    this.filteredTags = items;
    this.dataIsReady = true;
  }

  public itemsPerPageChanged() {
    this.dataIsReady = false;
    this.p = 1;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.p = page;
    this.getTagsValues();
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  private filterTags() {

    if (this.selectedDeviceId == -1 && this.selectedObjectType == -1)
      this.filteredTags = this.tags;
    else if (this.selectedDeviceId == -1 && this.selectedObjectType != -1) {
      this.filteredTags = this.tags;
      this.filteredTags = this.tags.filter((tag: Tag) => tag.objectType == this.selectedObjectType);
    }
    else if (this.selectedDeviceId != -1 && this.selectedObjectType == -1) {
      this.filteredTags = this.tags;
      this.filteredTags = this.tags.filter((tag: Tag) => tag.deviceId == this.selectedDeviceId);
    }
    else
      this.filteredTags = this.tags.filter((tag: Tag) => tag.deviceId == this.selectedDeviceId && tag.objectType == this.selectedObjectType);
  }

  public onSort(items: Tag[]) {
    this.p = 1;
    this.filteredTags = items;
    this.dataIsReady = true;
  }

  public onSearch(items: Tag[]) {
    this.p = 1;
    this.selectedDeviceId = -1;
    this.selectedDevice = null;
    this.selectedObjectType = -1;
    this.filteredTags = items;
    this.dataIsReady = true;
  }

  public searchStarted() {
    this.filterHeaderService.clearSearch();
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }
}