import { LogService } from './../../../services/log.service';
import { DeviceService } from 'src/app/services/device.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Device } from 'src/app/models/device';
import { Log } from 'src/app/models/log';
import { FilterSearchComponent } from '../../filter-search/filter-search.component';
declare var $: any;

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  @ViewChild(FilterSearchComponent) filterSearchComponent!: FilterSearchComponent;

  public p: number = 1;
  public itemsPerPage: number = 1000;
  public logs: Log[] = [];
  public filteredLogs: Log[] = [];
  public dataIsReady: boolean = false;
  public itemsPerPageOptions: any[] = [10, 25, 50, 100, 1000];

  public devices: Device[] = [];
  public objectTypes: number[] = [1, 2, 3, 4];
  public selectedObjectType: number = -1;
  public selectedDeviceId: number = -1;
  public selectedLog!: Log;

  constructor(private deviceService:DeviceService, 
    private logService:LogService) { }

  ngOnInit() {
    this.getData();
  }

  private getData() {
    forkJoin([this.logService.getLogs(), this.deviceService.getDevices()])
      .subscribe(
        (response: [Log[], Device[]]) => {
          this.logs = response[0];
          this.devices = response[1];
          this.filterLogs();

          // // if deleted last one
          // if (this.p > Math.round(this.filteredLogs.length / this.itemsPerPage))
          //   this.p = this.p > 1 ? --this.p : 1;

          this.dataIsReady = true
        },
        (error: string) => {

        });
  }


  public objectTypeChanged() {
    this.dataIsReady = false;
    this.p = 1;
    this.filterLogs();
    this.filterSearchComponent.clearSearch();
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public deviceChanged() {
    this.dataIsReady = false;
    this.p = 1;
    this.filterSearchComponent.clearSearch();
    this.selectedObjectType = -1;
    this.filterLogs();
    this.setObjectTypes();
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  private setObjectTypes() {
    this.objectTypes = [...new Set(this.filteredLogs.map((log: Log) => log.objectType))];
  }


  private filterLogs() {

    if (this.selectedDeviceId == -1 && this.selectedObjectType == -1)
      this.filteredLogs = this.logs;
    else if (this.selectedDeviceId == -1 && this.selectedObjectType != -1) {
      this.filteredLogs = this.logs;
      this.filteredLogs = this.logs.filter((log: Log) => log.objectType == this.selectedObjectType);
    }
    else if (this.selectedDeviceId != -1 && this.selectedObjectType == -1) {
      this.filteredLogs = this.logs;
      this.filteredLogs = this.logs.filter((log: Log) => log.deviceId == this.selectedDeviceId);
    }
    else
      this.filteredLogs = this.logs.filter((log: Log) => log.deviceId == this.selectedDeviceId && log.objectType == this.selectedObjectType);
  }

  public itemsPerPageChanged() {
    this.dataIsReady = false;
    this.p = 1;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public onSearch(filteredItems: Log[]) {
    this.p = 1;
    this.filteredLogs = filteredItems;
    this.dataIsReady = true;
  }

  public searchStarted() {
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.p = page;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }
}
