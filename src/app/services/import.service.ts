import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class ImportService {

  constructor(private dataService: DataService) { }

  public import(type: string, formData: FormData): Observable<any[]> {
    this.dataService.apiName = type + '/import';
    return this.dataService.uploadFile(formData);
  }

}
