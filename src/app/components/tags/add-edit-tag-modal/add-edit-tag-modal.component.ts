import { ModbusService } from './../../../services/modbus.service';
import { ModalMessage } from './../../../models/modal-message';
import { ObjectTypePipe } from './../../../pipes/object-type.pipe';
import { DeviceService } from 'src/app/services/device.service';
import { ModalService } from './../../../services/modal.service';
import { TagService } from './../../../services/tag.service';
import { Tag } from './../../../models/tag';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Device } from 'src/app/models/device';
import { forkJoin, SubscriptionLike } from 'rxjs';
import { Location } from "@angular/common";
declare var $: any;

@Component({
  selector: 'app-add-edit-tag-modal',
  templateUrl: './add-edit-tag-modal.component.html',
  styleUrls: ['./add-edit-tag-modal.component.css'],
  providers: [ObjectTypePipe]
})
export class AddEditTagModalComponent implements OnInit {

  public tagForm: FormGroup = this.fb.group({
    tagName: ['', Validators.required],
    deviceId: [null, Validators.required],
    objectType: [1, Validators.required],
    id: [1, Validators.required],
    indexArray: [1, [Validators.required, Validators.min(1)]],
    bitOffset: [{ value: 0, disabled: false }, Validators.required],
    amount: [{ value: 0, disabled: true }, [Validators.required, Validators.min(0), Validators.max(16)]],
    calculator: ['X', Validators.required],
    timeSearch: [20, Validators.required],
    minValueToWrite: [{ value: 0, disabled: true }, [Validators.required, Validators.min(0)]],
    maxValueToWrite: [{ value: 0, disabled: true }, [Validators.required, Validators.max(65535)]],
    enableWriting: [{ value: false, disabled: true }, Validators.required],
  });

  public objectTypeOptions: number[] = [1, 2, 3, 4];
  public readOptions: number[] = [1, 2, 4, 8, 16, 32, 64, 128];
  public bitOffsetOptions: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
  public idOptions: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
  public calculatorOptions: string[] = ['X', 'X / 10', 'X / 100', 'X / 1000', 'X * 10', 'X * 100', 'X * 1000'];
  public devices: Device[] = [];
  public isLoading: boolean = false;
  public dataIsReady: boolean = false;
  public isAddMode: boolean = false;
  public amountError: boolean = false;
  public tagId?: number;
  public lastAddedTagId?: number;
  public errorMessage: string = '';

  private locationSubscription: SubscriptionLike;

  constructor(private tagService: TagService,
    private deviceService: DeviceService,
    private modalService: ModalService,
    private objectTypePipe: ObjectTypePipe,
    private modbusService: ModbusService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder) {
    this.locationSubscription = this.location.subscribe(location => {
      $('#addEditModal').modal('hide');
    });
  }

  ngOnInit() {
    $('#addEditModal').modal('show');
    let httpRequestArray: any[] = [this.deviceService.getDevices()];

    // edit
    if (this.route.snapshot.paramMap.get('id')) {
      this.tagId = +this.route.snapshot.paramMap.get('id')!;
      httpRequestArray.push(this.tagService.getTagByTagId(this.tagId));
    }
    else {
      // get the last added tag
      if (this.route.snapshot.queryParamMap.get('lastAddedTagId')) {
        this.lastAddedTagId = +this.route.snapshot.queryParamMap.get('lastAddedTagId')!;
        httpRequestArray.push(this.tagService.getTagByTagId(this.lastAddedTagId));
      }
      this.isAddMode = true;
    }


    forkJoin(httpRequestArray)
      .subscribe(
        (response: any[]) => {
          this.devices = response[0];

          if (!this.isAddMode || (this.lastAddedTagId && response[1]))
            this.initializeForm(response[1]);
          else {
            this.tagForm.controls['deviceId'].setValue(this.devices[0].deviceId);
            this.setTagName();
          }

          this.dataIsReady = true;
        },
        (error: string) => {
          this.isLoading = false;
        });
  }

  ngOnDestroy() {
    this.locationSubscription.unsubscribe();
  }

  public navigate() {
    $('#addEditModal').modal('hide');
    setTimeout(() => {
      if (this.isAddMode)
        this.router.navigate(['../'], { relativeTo: this.route });
      else
        this.router.navigate(['../../'], { relativeTo: this.route });
    }, 150);
  }

  private initializeForm(tag: Tag) {
    this.tagForm.setValue({
      tagName: tag.tagName,
      deviceId: tag.deviceId,
      objectType: tag.objectType,
      id: tag.id,
      indexArray: tag.indexArray,
      bitOffset: tag.bitOffset,
      amount: tag.amount,
      calculator: tag.calculator,
      timeSearch: tag.timeSearch,
      minValueToWrite: tag.minValueToWrite,
      maxValueToWrite: tag.maxValueToWrite,
      enableWriting: tag.enableWriting,
    });

    switch (+this.tagForm.controls['objectType'].value) {
      // analog input
      case 1:
        this.tagForm.controls['bitOffset'].enable();
        this.tagForm.controls['minValueToWrite'].disable();
        this.tagForm.controls['maxValueToWrite'].disable();
        this.tagForm.controls['enableWriting'].disable();
        this.tagForm.controls['amount'].disable();
        this.tagForm.controls['amount'].setValue(0);
        break;
      // analog output
      case 2:
        this.tagForm.controls['minValueToWrite'].enable();
        this.tagForm.controls['maxValueToWrite'].enable();
        this.tagForm.controls['bitOffset'].enable();
        this.tagForm.controls['enableWriting'].enable();
        this.tagForm.controls['enableWriting'].setValue(true);
        this.tagForm.controls['amount'].enable();
        break;
      // digital input
      case 3:
        this.tagForm.controls['bitOffset'].disable();
        this.tagForm.controls['minValueToWrite'].disable();
        this.tagForm.controls['maxValueToWrite'].disable();
        this.tagForm.controls['enableWriting'].disable();
        this.tagForm.controls['enableWriting'].setValue(false);
        this.tagForm.controls['amount'].disable();
        this.tagForm.controls['amount'].setValue(0);
        break;
      // digital output
      case 4:
        this.tagForm.controls['bitOffset'].disable();
        this.tagForm.controls['minValueToWrite'].disable();
        this.tagForm.controls['maxValueToWrite'].disable();
        this.tagForm.controls['enableWriting'].enable();
        this.tagForm.controls['enableWriting'].setValue(true);
        this.tagForm.controls['amount'].disable();
        this.tagForm.controls['amount'].setValue(0);
        break;
      default:
        break;
    }
  }

  public deviceChanged() {
    this.setTagName();
  }

  public idChanged() {
    this.setTagName();
  }

  public indexArrayChanged() {
    this.setTagName();
  }

  public bitOfffsetChanged() {
    const bitOffset = +this.tagForm.controls['bitOffset'].value;
    const objectType = +this.tagForm.controls['objectType'].value;
    const amount = +this.tagForm.controls['amount'].value;

    if (bitOffset === 0)
      this.tagForm.controls['amount'].setValue(0);
    else if (objectType === 1)
      this.tagForm.controls['amount'].setValue(1);
    else if (objectType === 2 && amount === 0)
      this.tagForm.controls['amount'].setValue(1);

    this.setTagName();
    this.amountChanged();
  }

  public amountChanged() {
    const amount = +this.tagForm.controls['amount'].value;
    const bitOffset = +this.tagForm.controls['bitOffset'].value;
    const dif = 17 - bitOffset;

    if (amount > dif || amount === 0 && bitOffset > 0)
      this.amountError = true;
    else
      this.amountError = false;
  }

  public objectTypeChanged() {
    let value = +this.tagForm.value.objectType;

    switch (value) {
      // analog input
      case 1:
        this.tagForm.controls['bitOffset'].enable();
        this.tagForm.controls['enableWriting'].disable();
        this.tagForm.controls['enableWriting'].setValue(false);
        this.tagForm.controls['minValueToWrite'].disable();
        this.tagForm.controls['maxValueToWrite'].disable();
        this.tagForm.controls['minValueToWrite'].setValue(0);
        this.tagForm.controls['maxValueToWrite'].setValue(0);
        this.tagForm.controls['amount'].disable();
        this.tagForm.controls['amount'].setValue(0);
        break;
      // analog output
      case 2:
        this.tagForm.controls['minValueToWrite'].setValue(0);
        this.tagForm.controls['maxValueToWrite'].setValue(65535);
        this.tagForm.controls['enableWriting'].setValue(true);
        this.tagForm.controls['minValueToWrite'].enable();
        this.tagForm.controls['maxValueToWrite'].enable();
        this.tagForm.controls['bitOffset'].enable();
        this.tagForm.controls['enableWriting'].enable();
        this.tagForm.controls['amount'].enable();

        break;
      // digital input
      case 3:
        this.tagForm.controls['bitOffset'].disable();
        this.tagForm.controls['bitOffset'].setValue(0);
        this.tagForm.controls['enableWriting'].disable();
        this.tagForm.controls['enableWriting'].setValue(false);
        this.tagForm.controls['minValueToWrite'].disable();
        this.tagForm.controls['maxValueToWrite'].disable();
        this.tagForm.controls['minValueToWrite'].setValue(0);
        this.tagForm.controls['maxValueToWrite'].setValue(0);
        this.tagForm.controls['amount'].disable();
        this.tagForm.controls['amount'].setValue(0);
        break;
      // digital output
      case 4:
        this.tagForm.controls['bitOffset'].disable();
        this.tagForm.controls['bitOffset'].setValue(0);
        this.tagForm.controls['minValueToWrite'].setValue(0);
        this.tagForm.controls['maxValueToWrite'].setValue(1);
        this.tagForm.controls['minValueToWrite'].disable();
        this.tagForm.controls['maxValueToWrite'].disable();
        this.tagForm.controls['enableWriting'].enable();
        this.tagForm.controls['enableWriting'].setValue(true);
        this.tagForm.controls['amount'].disable();
        this.tagForm.controls['amount'].setValue(0);
        break;

      default:
        break;
    }

    this.setTagName();
  }

  public enableWritingChanged() {
    let enableWriting = this.tagForm.value.enableWriting;

    switch (+this.tagForm.value.objectType) {
      // digital input
      case 2:
        if (enableWriting) {
          this.tagForm.controls['minValueToWrite'].setValue(0);
          this.tagForm.controls['maxValueToWrite'].setValue(65535);
          this.tagForm.controls['minValueToWrite'].enable();
          this.tagForm.controls['maxValueToWrite'].enable();
        }
        else {
          this.tagForm.controls['minValueToWrite'].setValue(0);
          this.tagForm.controls['maxValueToWrite'].setValue(0);
          this.tagForm.controls['minValueToWrite'].disable();
          this.tagForm.controls['maxValueToWrite'].disable();
        }
        break;
      // digital output
      case 4:
        if (enableWriting)
          this.tagForm.controls['maxValueToWrite'].setValue(1);
        else
          this.tagForm.controls['maxValueToWrite'].setValue(0);
        break;
      default:
        this.tagForm.controls['minValueToWrite'].setValue(0);
        this.tagForm.controls['maxValueToWrite'].setValue(0);
        break;
    }
  }

  private setTagName() {
    if (this.isAddMode) {
      let deviceId = +this.tagForm.value.deviceId;
      let device: Device = this.devices.find((device: Device) => device.deviceId == deviceId)!;
      let objectTypeName = this.objectTypePipe.transform(+this.tagForm.value.objectType);
      let id = +this.tagForm.value.id;
      let indexArray = +this.tagForm.value.indexArray;

      // get also the disabled values from the form
      let bitOffset = +this.tagForm.getRawValue().bitOffset;

      let tagName = `${device.deviceName}_${objectTypeName}_${id}.${indexArray ? indexArray : ''}/${bitOffset}`;
      this.tagForm.controls['tagName'].setValue(tagName);
    }
  }

  private updateModbusDevice(deviceId: number) {
    this.modbusService.updateDevice(deviceId)
      .subscribe(
        (response: any) => {

        },
        (error: string) => {

        });
  }

  public onSubmit() {
    this.isLoading = true;
    this.errorMessage = '';
    let httpRequest: Observable<Tag>;

    if (this.tagId)
      httpRequest = this.tagService.editTag(this.tagId, this.tagForm.getRawValue());
    else
      httpRequest = this.tagService.addTag(this.tagForm.getRawValue());

    httpRequest
      .subscribe(
        (tag: Tag) => {
          this.updateModbusDevice(tag.deviceId);
          this.modalService.changesMade(new ModalMessage(this.isAddMode ? 'add' : 'edit', tag));
          this.navigate();
        },
        (error: string) => {
          this.errorMessage = error;
          this.isLoading = false;
        });
  }
}
