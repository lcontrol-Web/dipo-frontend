import { LoggedUser } from './models/logged-user';
import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public loggedIn: boolean = false;
 
  constructor(private authService: AuthService) {
    this.authService.loginStatus
      .subscribe(
        (loggedIn: boolean) => {
          this.loggedIn = loggedIn;
        });
  }
}
