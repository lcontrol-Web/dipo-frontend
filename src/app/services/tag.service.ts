import { DuplicateTag } from './../models/tag';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tag } from '../models/tag';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private dataService: DataService) { }

  public getTags(): Observable<Tag[]> {
    this.dataService.apiName = 'tags';
    return this.dataService.getData();
  }

  public getTagByTagId(tagId: number): Observable<Tag> {
    this.dataService.apiName = 'tags';
    return this.dataService.getDataByParam(tagId);
  }

  public addTag(tag: Tag): Observable<Tag> {
    this.dataService.apiName = 'tags';
    return this.dataService.add(tag);
  }

  public editTag(tagId: number, tag: Tag): Observable<Tag> {
    this.dataService.apiName = 'tags';
    return this.dataService.edit(tagId, tag);
  }

  public import(tags: any[]): Observable<any[]> {
    this.dataService.apiName = 'tags/import';
    return this.dataService.add(tags);
  }

  public duplicate(duplicateTag: DuplicateTag): Observable<any[]> {
    this.dataService.apiName = 'tags/duplicate';
    return this.dataService.add(duplicateTag);
  }
}