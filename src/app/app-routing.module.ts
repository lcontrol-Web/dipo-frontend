import { LogComponent } from './components/logs/log/log.component';
import { DuplicateTagModalComponent } from './components/tags/duplicate-tag-modal/duplicate-tag-modal.component';
import { WriteTagModalComponent } from './components/tags/write-tag-modal/write-tag-modal.component';
import { ConvertorComponent } from './components/convertor/convertor.component';
import { AddEditDeviceModalComponent } from './components/devices/add-edit-device-modal/add-edit-device-modal.component';
import { AddEditTagModalComponent } from './components/tags/add-edit-tag-modal/add-edit-tag-modal.component';
import { LoggedGuard } from './guards/logged.guard';
import { TagComponent } from './components/tags/tag/tag.component';
import { DeviceComponent } from './components/devices/device/device.component';
import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ImportModalComponent } from './components/import-modal/import-modal.component';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';
import DeactivateGuard from './guards/deactivate.guard';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [LoggedGuard] },
  {
    path: 'devices',
    component: DeviceComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'add', component: AddEditDeviceModalComponent },
      { path: 'edit/:id', component: AddEditDeviceModalComponent },
      { path: 'import', component: ImportModalComponent },
      { path: 'delete', component: DeleteModalComponent },
    ]
  },
  {
    path: 'tags',
    component: TagComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'add', component: AddEditTagModalComponent },
      { path: 'edit/:id', component: AddEditTagModalComponent },
      { path: 'write/:id', component: WriteTagModalComponent },
      { path: 'duplicate/:id', component: DuplicateTagModalComponent },
      { path: 'import', component: ImportModalComponent },
      { path: 'delete', component: DeleteModalComponent },
    ]
  },
  {
    path: 'convertor',
    component: ConvertorComponent,
    canActivate: [AuthGuard],
    canDeactivate: [DeactivateGuard],
    children: [
      { path: 'import', component: ImportModalComponent }
    ]
  },
  {
    path: 'logs',
    component: LogComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
