import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public apiName: string = '';

  constructor(private http: HttpClient) { }

  public getData(params?: any): Observable<any> {

    if (!params) {
      params = new HttpParams();
      params = params.append('timestamp', new Date().getTime());
    }

    return this.http.get(environment.apiUrl + '/api/' + this.apiName, { params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataByParam(paramValue: string | number, params?: any): Observable<any> {

    if (!params) {
      params = new HttpParams();
      params = params.append('timestamp', new Date().getTime());
    }

    return this.http.get(environment.apiUrl + '/api/' + this.apiName + '/' + paramValue, { params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataByObject(data: any, params?: any): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(environment.apiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers, params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }))
  }

  public add(data: any): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(environment.apiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers }).pipe(
      map((response: any) => {
        return response;
      }));
  }

  public edit(paramValue: string | number, data: any): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.put(environment.apiUrl + '/api/' + this.apiName + '/' + paramValue, JSON.stringify(data), { headers: headers }).pipe(
      map(
        (response: any) => {
          return response
        }));
  }

  public delete(paramValue: string | number): Observable<any> {

    return this.http.delete(environment.apiUrl + '/api/' + this.apiName + '/' + paramValue).pipe(
      map(
        (response: any) => {
          return response
        }));
  }

  public multipleDelete(data: string[] | number[]): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(environment.apiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers }).pipe(
      map(
        (response: any) => {
          return response
        }));
  }

  public uploadFile(data: FormData): Observable<any> {
    return this.http.post(environment.apiUrl + '/api/' + this.apiName, data);
  }

  public getApiName(): string {
    return this.apiName;
  }
}
