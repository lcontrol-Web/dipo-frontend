export class ModalMessage {
    public modalType: string = '';
    public item: any;

    constructor(modalType: string, item: any) {
        this.modalType = modalType;
        this.item = item;
    }
}
