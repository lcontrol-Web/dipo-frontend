export class Device {
    public deviceId: number = 0;
    public deviceName: string = '';
    public ip: string = '';
    public port: number = 502;
    public timeSearch: number = 400;
    public timeOut: number = 10;
    public protocol: string = '';
    public maxRegisterToRead: number = 16;
    public enable: boolean = true;
    public status: boolean = false;
    public isEnableLoading?: boolean = false;

    constructor() {

    }

}
export class DeviceStatus {
    public deviceId: number = 0;
    public status: boolean = false;

    constructor() {

    }

}
