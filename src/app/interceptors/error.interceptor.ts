import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService,
        private router: Router,) { }

    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {

                if(error.status == 0){
                    return throwError('ServerIsDown');
                }

                // for unauthorized erros
                if (error.status === 401) {
                    this.authService.logout();
                    this.router.navigate(['']);
                }
                const err = error.error || error.statusText;

                if (err === "ExpiredToken") {
                    this.authService.logout();
                    this.router.navigate(['']);
                }
                
                return throwError(err);
            })
        );

    }

}
