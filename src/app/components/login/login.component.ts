import { Credentials } from './../../models/credentials';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public isLoading: boolean = false;
  public loginError: boolean = false;
  public showPassword: boolean = false;

  public loginForm = new FormGroup({
    userName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    remember: new FormControl(true)
  })

  constructor(private authService: AuthService,
    private cookieService: CookieService,
    private router: Router) {

    if (this.cookieService.check('userCredentials')) {
      const credentials: Credentials = <Credentials>JSON.parse(this.cookieService.get('userCredentials'));
      if (credentials)
        this.loginForm.setValue({
          userName: credentials.userName,
          password: credentials.password,
          remember: true
        })
    }
  }

  ngOnInit() { }

  public passwordPressed(){
    this.showPassword = !this.showPassword;
  }

  public onSubmit() {
    this.loginError = false;
    this.isLoading = true;
    this.authService.login(this.loginForm.value)
      .subscribe(
        (response: boolean) => {
          if (response)
            this.router.navigate(['devices']);
          else
            this.loginError = true;
        },
        (error: string) => {
          this.loginError = true;
          this.isLoading = false;
        })
  }
}
