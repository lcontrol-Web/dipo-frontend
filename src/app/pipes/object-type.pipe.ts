import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectTypePipe'
})
export class ObjectTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case 1:
        return 'Analog input'
      case 2:
        return 'Analog output'
      case 3:
        return 'Digital input'
      case 4:
        return 'Digital output'
      default:
        return 'Object type not exist'
    }
  }
}
