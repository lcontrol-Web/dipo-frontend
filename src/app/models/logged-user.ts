export class LoggedUser {
    public autoId: number = 0;
    public userName: string = '';
    public token: string = '';

    constructor() {

    }
}
