import { ModalMessage } from './../models/modal-message';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private modalSubject = new Subject();

  constructor() { }

  public changesMade(message: ModalMessage) {
    this.modalSubject.next(message);
  }

  clearMessages() {
    this.modalSubject.next();
  }

  public onChangesMade(): Observable<any> {
    return this.modalSubject.asObservable();
  }

}
