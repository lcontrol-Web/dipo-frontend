import { FilterHeaderService } from './../../../services/filter-header.service';
import { ModbusService } from './../../../services/modbus.service';
import { ModalService } from 'src/app/services/modal.service';
import { Router } from '@angular/router';
import { DeviceService } from './../../../services/device.service';
import { Device, DeviceStatus } from './../../../models/device';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { ModalMessage } from 'src/app/models/modal-message';
import { interval } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { FilterSearchComponent } from '../../filter-search/filter-search.component';
declare var $: any;

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  @ViewChild(FilterSearchComponent) filterSearchComponent!: FilterSearchComponent;

  public p: number = 1;
  public itemsPerPage: number = 1000;
  public devices: Device[] = [];
  public filteredDevices: Device[] = [];
  public dataIsReady: boolean = false;
  public isEnableLoading: boolean = false;
  public itemsPerPageOptions: any[] = [10, 25, 50, 100, 1000];

  private modalSubscription: Subscription;
  private deviceStatusInterval!: Subscription;

  constructor(private deviceService: DeviceService,
    private modbusService: ModbusService,
    private modalService: ModalService,
    private filterHeaderService: FilterHeaderService,
    private router: Router) {

    this.modalSubscription = this.modalService.onChangesMade()
      .subscribe((message: ModalMessage) => {
        this.dataIsReady = false;
        this.filterSearchComponent.clearSearch();
        this.filterHeaderService.clearSearch();
        this.getDevices();
      });
  }

  ngOnInit() {
    this.getDevices();
  }

  ngOnDestroy() {
    if (this.modalSubscription)
      this.modalSubscription.unsubscribe();

    if (this.deviceStatusInterval)
      this.deviceStatusInterval.unsubscribe();
  }

  private getDevices() {
    this.deviceService.getDevices()
      .subscribe(
        (devices: Device[]) => {
          this.devices = devices;
          this.filteredDevices = this.devices;

          if (this.p > Math.round(this.filteredDevices.length / this.itemsPerPage))
            this.p = this.p > 1 ? --this.p : 1;

          this.getDevicesStatus();
          this.dataIsReady = true;
        },
        (error: string) => {

        });
  }

  private getDevicesStatus() {
    if (this.deviceStatusInterval)
      this.deviceStatusInterval.unsubscribe();

    this.deviceStatusInterval = interval(3000)
      .pipe(
        startWith(0),
        switchMap(() => this.modbusService.getDevicesStatus()))
      .subscribe(
        (devicesStatus: DeviceStatus[]) => {
          for (const deviceStatus of devicesStatus)
            for (let device of this.devices)
              if (deviceStatus.deviceId == device.deviceId)
                device.status = deviceStatus.status;
        },
        (error: string) => {
          for (const device of this.devices)
            device.status = false;

          setTimeout(() => {
            this.getDevicesStatus()
          }, 5000);
        })
  }

  public enableChanged(device: Device) {
    this.isEnableLoading = true;
    device.isEnableLoading = true;
    let tempDevice: Device = JSON.parse(JSON.stringify(device));
    delete tempDevice.isEnableLoading;
    this.deviceService.editDevice(tempDevice.deviceId, tempDevice)
      .subscribe(
        (devices: Device) => {
          this.updateModbusDevice(device);
        },
        (error: string) => {
          device.enable = !device.enable;
          this.isEnableLoading = false;
          device.isEnableLoading = false;
        });
  }

  public addDevice() {
    this.router.navigate(['/devices/add']);
  }

  public editDevice(device: Device) {
    this.router.navigate(['/devices/edit', device.deviceId]);
  }

  public import() {
    this.router.navigate(['/devices/import'], {
      queryParams: {
        type: 'devices',
      }
    });
  }

  public deleteDevice(device: Device) {
    this.router.navigate(['/devices/delete'], {
      queryParams: {
        type: 'devices',
        id: device.deviceId,
        name: device.deviceName
      }
    });
  }

  private updateModbusDevice(device: Device) {
    this.modbusService.updateDevice(device.deviceId)
      .subscribe(
        (response: any) => {
          device.isEnableLoading = false;
          this.isEnableLoading = false;
        },
        (error: string) => {
          device.isEnableLoading = false;
          this.isEnableLoading = false;
        });
  }

  public filterByEnabled(event: any, enable?: boolean) {
    $('.enable-filter-dropdown-menu .dropdown-item').removeClass("active");
    $(event.target).addClass("active");

    if (enable == null)
      this.filteredDevices = this.devices;
    else {
      this.filteredDevices = this.devices.filter((device: Device) => {
        return device.enable == enable
      });
    }
  }

  public filterByStatus(event: any, status?: boolean) {
    $('.status-filter-dropdown-menu .dropdown-item').removeClass("active");
    $(event.target).addClass("active");

    if (status == undefined)
      this.filteredDevices = this.devices;
    else {
      this.filteredDevices = this.devices.filter((device: Device) => {
        if (!device.hasOwnProperty('status') && !status)
          return device;
        else
          return device.status == status
      });
    }
  }

  public onFilterHeaderStart() {
    this.filterSearchComponent.clearSearch();
    this.dataIsReady = false;
  }

  public onFilterHeaderFinish(items: any[]) {
    this.p = 1;
    this.filteredDevices = items;
    this.dataIsReady = true;
  }

  public itemsPerPageChanged() {
    this.dataIsReady = false;
    this.p = 1;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }

  public onSearch(filteredItems: Device[]) {
    this.p = 1;
    this.filteredDevices = filteredItems;
    this.dataIsReady = true;
  }

  public searchStarted() {
    this.filterHeaderService.clearSearch();
    this.dataIsReady = false;
  }

  public searchEnded() {
    this.dataIsReady = true;
  }

  public pageChanged(page: number) {
    this.dataIsReady = false;
    this.p = page;
    $(document).ready(() => {
      this.dataIsReady = true;
    });
  }
}
