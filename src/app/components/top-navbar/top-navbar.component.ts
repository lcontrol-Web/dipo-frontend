import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {

  public loggedIn: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.loginStatus
      .subscribe(
        (loggedIn: boolean) => {
          this.loggedIn = loggedIn;
        });
  }

  public logout() {
    this.authService.logout();
  }
}
