import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataModbusService {
  public apiName: string = '';

  constructor(private http: HttpClient) { }

  public getData(params?: any): Observable<any> {

    if (!params) {
      params = new HttpParams();
      params = params.append('timestamp', new Date().getTime());
    }

    return this.http.get(environment.serviceApiUrl + '/api/' + this.apiName, {
      params: params
    }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataByParam(paramValue: string | number, params?: any): Observable<any> {

    if (!params) {
      params = new HttpParams();
      params = params.append('timestamp', new Date().getTime());
    }

    return this.http.get(environment.serviceApiUrl + '/api/' + this.apiName + '/' + paramValue, { params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }));
  }

  public getDataByObject(data: any, params?: any): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(environment.serviceApiUrl + '/api/' + this.apiName, JSON.stringify(data), { headers: headers, params: params }).pipe(
      map(
        (response: any) => {
          return response;
        }))
  }
}
