import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-filter-search',
  templateUrl: './filter-search.component.html',
  styleUrls: ['./filter-search.component.css']
})
export class FilterSearchComponent implements OnInit {

  @Input() items: any[] = [];
  @Input() fields: string[] = [];

  @Output() private onStartSearch: EventEmitter<any> = new EventEmitter();
  @Output() private onEndSearch: EventEmitter<any> = new EventEmitter();
  @Output() private onSearch: EventEmitter<any[]> = new EventEmitter();

  public searchValue: string = '';
  private searchTextChanged = new Subject<string>();
  private searchSubscription!: Subscription;

  constructor() {
    this.subscribeToSearch();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.searchSubscription)
      this.searchSubscription.unsubscribe();
  }

  private subscribeToSearch() {
    this.searchSubscription = this.searchTextChanged.pipe(
      debounceTime(1000),
      distinctUntilChanged((prev, curr) => {
        if (prev == curr) {
          this.onEndSearch.emit();
          return true;
        }
        return false;
      }))
      .subscribe(() => {
        const filteredItems = this.items.filter(item => {
          for (let field of this.fields)
            if (item[field] && item[field].toString().toLowerCase().includes(this.searchValue.toLowerCase()))
              return item;
        });

        this.onSearch.emit(filteredItems);
      });
  }

  public searchValueChanged() {
    this.onStartSearch.emit();
    this.searchTextChanged.next(this.searchValue);
  }

  public clearSearch() {
    this.searchValue = '';

    // reset subscription
    this.searchSubscription.unsubscribe();
    this.subscribeToSearch();
  }

}
