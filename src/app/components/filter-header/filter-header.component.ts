import { FilterHeaderMessage } from './../../models/filter-header-message';
import { FilterHeaderService } from './../../services/filter-header.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { FilterAction } from 'src/app/enums/filter-action';
import { DataTypes } from 'src/app/enums/data-types.enum';
declare var $: any;

@Component({
  selector: 'app-filter-header',
  templateUrl: './filter-header.component.html',
  styleUrls: ['./filter-header.component.css']
})
export class FilterHeaderComponent implements OnInit {

  @Input() public headerName: string = '';
  @Input() public filterNumber: number = 0;
  @Input() public column: string = '';
  @Input() public items: any[] = [];
  @Input() public type: string = DataTypes.String;
  @Output() public onFilterStart = new EventEmitter();
  @Output() public onFilterFinish = new EventEmitter<any[]>();
  @Output('onSort') sortEmitter = new EventEmitter<any[]>();

  public searchValue: string = '';
  public filterEnabled: boolean = false;
  public ascendingEnabled: boolean = false;
  public descendingEnabled: boolean = false;

  private subscription: Subscription;

  constructor(private filterHeaderService: FilterHeaderService) {
    this.subscription = this.filterHeaderService.getMessage()
      .subscribe(
        (message: FilterHeaderMessage) => {
          if (message.filterNumber != this.filterNumber) {
            switch (message.action) {
              case FilterAction.Filter:
                this.searchValue = '';
                this.filterEnabled = false;
                break;
              case FilterAction.Sort:
                this.ascendingEnabled = false;
                this.descendingEnabled = false;
                break;
              case FilterAction.Clear:
                this.ascendingEnabled = false;
                this.descendingEnabled = false;
                this.searchValue = '';
                this.filterEnabled = false;
                break;
              default:
                break;
            }

          }
        });
  }

  ngOnInit() { }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  public asc() {
    this.ascendingEnabled = true;
    this.descendingEnabled = false;
    $('#dropdownMenuButton' + this.filterNumber).dropdown('hide');
    const filteredItems = this.filterHeaderService.asc([...this.items], this.column, this.type);
    this.filterHeaderService.onSort(this.filterNumber);
    this.sortEmitter.emit(filteredItems);
  }

  public dsc() {
    this.descendingEnabled = true;
    this.ascendingEnabled = false;
    $('#dropdownMenuButton' + this.filterNumber).dropdown('hide');
    const filteredItems = this.filterHeaderService.dsc([...this.items], this.column, this.type);
    this.filterHeaderService.onSort(this.filterNumber);
    this.sortEmitter.emit(filteredItems);
  }

  filter() {
    this.filterEnabled = true;
    this.onFilterStart.emit();
    $('#dropdownMenuButton' + this.filterNumber).dropdown('hide');
    this.filterHeaderService.onFilter(this.filterNumber);

    $(document).ready(() => {
      try {
        let filteredItems: any[] = this.items.filter((item) => {
          if (!this.searchValue && !item[this.column])
            return true;
          else if (this.searchValue && item.hasOwnProperty(this.column) && item[this.column] && item[this.column].toString().toLowerCase().includes(this.searchValue))
            return true;
          return false;
        });
        this.onFilterFinish.emit(filteredItems);
      } catch (error) {
        console.log(error);
        this.onFilterFinish.emit(this.items);
      }
    });
  }

  clearFilter() {
    this.filterEnabled = false;
    $('#dropdownMenuButton' + this.filterNumber).dropdown('hide');
    this.onFilterStart.emit();
    this.searchValue = '';
    $(document).ready(() => {
      this.onFilterFinish.emit(this.items);
    });
  }

}
