import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { Credentials } from '../models/credentials';
import { LoggedUser } from '../models/logged-user';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public loginStatus = new BehaviorSubject<boolean>(this.hasToken());

  constructor(private http: HttpClient,
    public router: Router,
    private cookieService: CookieService) {
  }

  public login(credentials: Credentials): Observable<any> {
    return this.http.post<LoggedUser>(`${environment.apiUrl}/api/auth`, credentials, { observe: 'response' })
      .pipe(tap(
        (response: HttpResponse<LoggedUser>) => {
          if (response.headers.get('x-auth-token')) {
            this.cookieService.set('currentUser', response.headers.get('x-auth-token')!.toString());

            if (credentials.remember) {
              this.cookieService.set('userCredentials', JSON.stringify({
                userName: credentials.userName,
                password: credentials.password
              }));
            }
            else
              this.cookieService.delete('userCredentials');

            this.loginStatus.next(true);
            return true;
          }
          else
            return false;

          // let user = response.body;
          // if (user) {
          //   if (credentials.remember) {
          //     this.resetCredentials();
          //     localStorage.setItem('userCredentials', JSON.stringify(credentials));
          //   }
          //   else
          //     this.resetCredentials();

          //   localStorage.setItem('currentUser', JSON.stringify(user));
          //   this.currentUserSubject.next(user);
          //   return user;
          // }
          // else
          //   return null;
        }));
  }

  public logout() {
    this.cookieService.delete('currentUser');
    this.loginStatus.next(false);
    this.router.navigate(['']);
  }


  /**
    *
    * @returns {Observable<T>}
    */
  public isLoggedIn(): Observable<boolean> {
    return this.loginStatus.asObservable();
  }

  /**
   * if we have token the user is loggedIn
   * @returns {boolean}
   */
  private hasToken(): boolean {
    return this.cookieService.check('currentUser');
  }

}
