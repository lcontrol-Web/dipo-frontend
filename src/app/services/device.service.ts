import { DataService } from 'src/app/services/data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Device } from '../models/device';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private dataService: DataService) { }

  public getDevices(): Observable<Device[]> {
    this.dataService.apiName = 'devices';
    return this.dataService.getData();
  }

  public getDeviceByDeviceId(deviceId: number): Observable<Device> {
    this.dataService.apiName = 'devices';
    return this.dataService.getDataByParam(deviceId);
  }

  public addDevice(device: Device): Observable<Device> {
    this.dataService.apiName = 'devices';
    return this.dataService.add(device);
  }

  public editDevice(deviceId: number, device: Device): Observable<Device> {
    this.dataService.apiName = 'devices';
    return this.dataService.edit(deviceId, device);
  }
  
  public import(devices: any[]): Observable<any[]> {
    this.dataService.apiName = 'devices/import';
    return this.dataService.add(devices);
  }
}
