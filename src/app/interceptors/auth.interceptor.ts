import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor {

  constructor(private cookieService: CookieService) { }

  intercept(req: HttpRequest<any>,
    next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.cookieService.get('currentUser');

    if (token) {
      req = req.clone({
        setHeaders: {
          'x-auth-token': `${token}`
        }
      });
    }

    return next.handle(req);
  }

}
